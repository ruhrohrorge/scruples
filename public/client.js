function getRouteCameras(from, to) {
    return $.get('/routecams', {
            from : from,
            to : to
    });
};

function getNearestCam(coords) {
    return $.get('/camera/' + coords.lat + '/' + coords.lng);
};

function getLocation() {
    var locationDeferred = $.Deferred();

    if (!navigator.geolocation){
        locationDeferred.reject("No geolocation api");
    } else {
        navigator.geolocation.getCurrentPosition(function(pos) {
            locationDeferred.resolve({
                lat : pos.coords.latitude,
                lng : pos.coords.longitude
            })
        });
    }
    return locationDeferred.promise();
}


var viewModel = {
    from : ko.observable('SW1V 2NS, London'),
    to : ko.observable('London, SW4 6EL'),
    route : function() {
        getRouteCameras(viewModel.from(), viewModel.to()).then(function(cameras){
            viewModel.cameras(cameras);
        });
    },
    cameras : ko.observable()
};

getLocation()
    .then(function(location){
        viewModel.from(location.lat + ',' + location.lng);
    });

ko.applyBindings(viewModel);