//var Rx = require('rx');


var core = require('./core');
var http = require('http');
var Enumerable = require('linq');
var Rx = require('rx');
var Q = require('q');
var xml2js = require('xml2js');
var xml2json = require('xml2json');
//var parser = xml2js.Parser();
var url = 'http://www.tfl.gov.uk/tfl/livetravelnews/trafficcams/cctv/jamcams-camera-list.xml';

function getDistanceFromLatLonInM(lat1,lon1,lat2,lon2) {
    var R = 6371; // Radius of the earth in km
    var dLat = deg2rad(lat2-lat1);  // deg2rad below
    var dLon = deg2rad(lon2-lon1);
    var a =
            Math.sin(dLat/2) * Math.sin(dLat/2) +
                Math.cos(deg2rad(lat1)) * Math.cos(deg2rad(lat2)) *
                    Math.sin(dLon/2) * Math.sin(dLon/2);

    var c = 2 * Math.atan2(Math.sqrt(a), Math.sqrt(1-a));
    var d = R * c; // Distance in km
    return d * 1000 ;
}

function deg2rad(deg) {
    return deg * (Math.PI/180)
}

function fetchXmlList() {
    var response = Q.defer();
    http.get(url, function(res) {
        var str = '';

        res.on('data', function (chunk) {
            str += chunk;
        });

        res.on('error', function(e) {
            response.reject(e);
        })

        res.on('end', function () {
            response.resolve(str);
        });
    });
    return response.promise;
}

function parseXml2 (str) {
    var response = Q.defer();

    xml2js.parseString(str, function (err, result) {
        if (err) {
            response.reject(err);
        } else {
            response.resolve(result);
        }
    });
    return response.promise;
};

function parseXml (str) {
    var response = Q.defer();
    var obj = xml2json.toJson(str, { object: true });
    response.resolve(obj);

    return response.promise;
};

function getClosestCamera(cameras, lat, lon) {
    var response = Q.defer();

    var kThreshold = 1000000000000; // 1000 m

    var closestCamera = Enumerable.from(cameras)
        .select(function(camera) {
            return {
                camera : camera,
                distance : getDistanceFromLatLonInM(lat, lon, camera.lat, camera.lng)
            };
        })
        .minBy(function(camera) {
            return camera.distance;
        });

    var result = (closestCamera.distance <= kThreshold) ? closestCamera : null;
    response.resolve(result);

    return response.promise;
}


var camerasList = [];
Rx.Observable
    .timer(0,5000)
    .select(fetchXmlList)
    .select(Rx.Observable.fromPromise)
    .switch()
    .select(parseXml)
    .select(Rx.Observable.fromPromise)
    .switch()
    .select(function(obj){
        return obj.syndicatedFeed.cameraList.camera;
    })
    .select(function(cameras) {
        return Enumerable.from(cameras)
            .where(function(camera) {
                return !camera.unavailabilityCause;
            });
    })
    .subscribe(function( camerasListNew ){
        camerasList = camerasListNew;
    });

exports.getClosestCamera= function(lat, lon)  {
    return getClosestCamera(camerasList,lat, lon);
}






