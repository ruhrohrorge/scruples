/**
 * Created by Conor on 16/02/2014.
 */

var gm = require('googlemaps');
var Enumerable = require('linq');
var Q = require('q');

gm.config('key', 'AIzaSyA61x2SVgLTCQoYv1vSqs_vjU7yarKUasQ');

var getDirections = function(from,to) {
    var response = Q.defer();
    gm.directions(from,to ,
        function(err, directions){
            if (err)
                response.reject(err);
            else
                response.resolve(directions);
        });
    return response.promise;
}

exports.getRouteCoordinatesArray = function (from, to) {
    return getDirections(from, to)
        .then(function (directions) {
            var steps = Enumerable.from(directions.routes)
                .selectMany('$.legs')
                .selectMany('$.steps')
                .toArray();
            return Enumerable.from([Enumerable.from(steps).first().start_location])
                .union(Enumerable.from(steps)
                    .select(function (x) {
                    return x.end_location
                }))
                .toArray();
        })
        .fail(function(err)
        {
            console.error(err);
        });
}
