
/**
 * Module dependencies.
 * this is CK testing again
 */

var urlLib = require('url');
var Q = require('q');
var express = require('express');
var routes = require('./routes');
var user = require('./routes/user');
var http = require('http');
var Enumerable = require('linq');
var path = require('path');
var jamcam = require('./services/jamcam');
var gmaps = require('./services/googlemaps');

var app = express();

// all environments
app.set('port', process.env.PORT || 3000);
app.use(express.favicon());
app.use(express.logger('dev'));
app.use(express.json());
app.use(express.urlencoded());
app.use(express.methodOverride());
app.use(app.router);
app.use(express.static(path.join(__dirname, 'public')));

// development only
if ('development' == app.get('env')) {
  app.use(express.errorHandler());
}

app.get('/', routes.index);
app.get('/users', user.list);

app.get('/camera/:lat/:lon', function(req, res){
    var lat = parseFloat(req.params.lat);
    var lon = parseFloat(req.params.lon);
    jamcam.getClosestCamera(lat,lon).then(function(cam){
        res.send(cam);
    });
});

//http://localhost:3000/routecams?from=marlow,%20united%20kingdom&to=45%20moreton%20terrace,%20london,%20sw1v%202ns
app.get('/routecams', function(req, res){
    var url_parts = urlLib.parse(req.url, true);
    var query = url_parts.query;
    if (!query.from || !query.to) {
        res.send('You\'re missing params chump!  The urlLib needs a from and to in the query string, e.g. ?=from=fromAddress&to=toAddress');
    } else {
        gmaps.getRouteCoordinatesArray(query.from,query.to)
            .then(function(coords) {
                var promises = Enumerable.from(coords)
                    .select(function(coord) {
                        return jamcam.getClosestCamera(coord.lat,coord.lng);
                    })
                    .toArray();
                return Q.all(promises);
            })
            .then(function(item) {
                var distinctCameras =  Enumerable.from(item)
                    .distinct(function(cameraPt) {
                        return cameraPt.camera.id;
                    }).toArray();
                res.send(distinctCameras);
            })
            .fail(function(err) {
                console.error(err);
            });
    }

});

http.createServer(app).listen(app.get('port'), function(){
  console.log('Express server listening on port ' + app.get('port'));
});
